class i2c_driver extends ovm_driver#(i2c_sequence_item);
i2c_sequence_item seq_item;
virtual i2c_intf intf;
Wrapper wrapper;
ovm_object dummy;

  bit read_write = 1'b0;
  bit stop= 1'b0; 
  bit start = 1'b0;;
  bit ack,ackfromslave; 
//registering
`ovm_component_utils(i2c_driver)
//constructor
function new(string name = " ", ovm_component parent = null);
super.new(name,parent);
endfunction: new


virtual function void build();
//casting
if(!get_config_object("configuration",dummy,0))
ovm_report_info(get_type_name(),"dummy is not assigned",OVM_LOG);
else
ovm_report_info(get_type_name(),"dummy is assigned",OVM_LOG); 


if($cast(wrapper,dummy))
  begin
ovm_report_info(get_type_name(),"object configured properly",OVM_LOG);
intf = wrapper.intf;
end
else
ovm_report_info(get_type_name(),"dummy is really dummy");

endfunction : build


//run phase
virtual task run ();

reset_dut();
		forever 
		begin
			
			 
				seq_item_port.get_next_item(seq_item);
				$display("i pulled the transaction");
				
				 $display(     $time );
				 				//slave_write(seq_item.addr,seq_item.data);
				//slave_write(seq_item.addr,seq_item.data);
				  slave_write(seq_item);
				  $display(     $time );
				  				$display("driver write address is %d write data is %d",seq_item.addr,seq_item.data);
            //    slave_read(seq_item.addr,seq_item.data); 
            $display(     $time );
             slave_read(seq_item); 
             $display(     $time );  
			 $display("driver read address is %d read data is %d",seq_item.addr,seq_item.data);
             $display(     $time );
			

			 //#50ns
			 
			seq_item_port.item_done();
							
					
					
				
			end
  
endtask : run

//task slave_write(input bit [6:0] addr,input bit [7:0] data );

task slave_write(i2c_sequence_item seq_item);
  
  intf.sda_in = 1'b1;
  intf.sck = 1'b1;
  #50ns
  intf.sda_in = 1'b0;
  #50ns 
  intf.sck = 1'b0;
  start = 1'b1;
 fork
   ClockGen(); //100khz 
  // datatransfer(addr,data);
  datatransfer (seq_item);
      join_any
 
 $display("out of fork loop");
 
 
 intf. sda_in = 0; 
 start = 0;
 #20ns
 intf.sck = 1;
 #50ns
 intf.sda_in = 1'b1;
 #500ns;
  

  
endtask : slave_write
  
task ClockGen();
  forever begin
  //    #0.01ms intf.sck = ~intf.sck;
         #200ns   intf.sck = ~intf.sck;
		            
  end
endtask : ClockGen

task reset_dut();
       intf.rst = 0;
       #50ns ;
       intf.rst = 1;
       intf.sck = 1;
       intf.sda_in = 1;
     endtask
   
   
//task datatransfer(input bit [6:0] addr,input bit [7:0] data);
task datatransfer(i2c_sequence_item seq_item);
  
    if( start ) 
	begin
	 for(int i = 6; i>=0;i--)
	  begin
	    @(posedge intf.sck)
	       intf.sda_in = seq_item.addr[i];
		  // $display("at driver sending write address %d",seq_item.addr[i]);
		   $display("value i is %d",i);
		   $display($time);
		   $display("at driver sending write address %d",intf.sda_in); 
		   end // for loop

        @ (posedge intf.sck)
           intf.sda_in = 1'b0;	  
		   $display("at driver sending write bit %d ",intf.sda_in);
		@ (posedge intf.sck)
            
			ack = intf.sda_out;
			$display("at driver ack from slave for  write address ack is %d",ack);  
			if (ack == 0)
          begin		
               @ (posedge intf.sck)
			   $display("ack received from slave ");
			   
			   @ (posedge intf.sck)
		      for (int k = 7; k>=0;k--)	  
			    begin
			     @ (posedge intf.sck);  
				  intf.sda_in = seq_item.data[k];
				  $display("at driver sending write data %d",seq_item.data[k]);
				  
				end
				end
	    @ (posedge intf.sck)		
		@ (posedge intf.sck)
		  ack = intf.sda_out;
          $display("at driver ack from slave for  write data   is %d",ack); 
		  
		
	//	else
		// $display("slave address wrong");
		 
		 
		
    end  

 endtask  

  
//task slave_read(input bit [6:0] addr,output bit [7:0] data );
  task slave_read(i2c_sequence_item seq_item);
  /*
  intf.sda_in = 1'b1;
  intf.sck = 1'b1;
  #50ns
  intf.sda_in = 1'b0;
  #50ns 
  intf.sck = 1'b0;
  */
  $display($time);
  intf.sda_in = 1'b0;
  start = 1'b1;
 fork
   ClockGen(); //100khz 
   //datareceive(addr,data);
   datareceive(seq_item  );
 join_any
 
 # 50ns
 intf. sda_in = 0;
 #50ns
 intf.sck = 1;
 #40
 intf.sda_in = 1'b1;
 start = 1'b0;

  
endtask : slave_read 

//task datareceive(input bit [6:0] addr, output bit [7:0] data);
task datareceive(i2c_sequence_item seq_item);

if( start ) 
	begin
	 for(int i = 6; i>=0;i--)
	  begin
	    @(posedge intf.sck)
	       intf.sda_in = seq_item.addr[i];
		  // $display("at driver sending write address %d",seq_item.addr[i]);
		   $display("value i is %d",i);
		   $display("at driver sending read address %d",intf.sda_in); 
		   end // for loop

        @ (posedge intf.sck)
           intf.sda_in = 1'b1;	  
		   $display("at driver sending read bit %d ",intf.sda_in);
		@ (posedge intf.sck)
            
			ack = intf.sda_out;
			$display("at driver ack from slave for  read address ack is %d",ack);  
			if (ack == 0)
          begin		
               @ (posedge intf.sck)
			   $display("ack received from slave ");
			   
			    
		      for (int k = 7; k>=0;k--)	  
			    begin
			     @ (posedge intf.sck);  
				   seq_item.data[k] = intf.sda_out;
				  $display("at driver reading data %d",seq_item.data[k]);
				  $display($time);
				  
				end
				end
	    @ (posedge intf.sck)		
		@ (posedge intf.sck)
		   intf.sda_in = 1 ;
          $display("at driver ack to slave for  read data   is %d",ack); 
		  
		
	//	else
		// $display("slave address wrong");
		 
		 
		
    end  

 
/* 
    if( start ) 
	begin
	 for(int i = 0; i<7;i++)
	  begin
	    @(negedge intf.sck)
	       intf.sda_in = seq_item.addr[i];
		   
	  end // for loop

        @ (negedge intf.sck)
           intf.sda_in = 1'b1;	  
		   
		@ (negedge intf.sck)
            
			ack = intf.sda_out;
			
        if (ack == 1)
          begin		
              $display("ack received from slave ");
		      for (int k = 0; k<8;k++)	  
			    begin
				  @ (posedge intf.sck) 
				  seq_item.data[k] = intf.sda_out ;
				end
				end
		else
		 $display("slave address wrong");
		 
		 
		
    end  
*/
 endtask : datareceive


endclass : i2c_driver 