class i2c_sequence_item extends ovm_sequence_item;
// variable declaration 

rand bit [6:0] addr; 
rand bit [7:0] data;
rand bit wr_rd;
rand bit ack;
 
 // registering with factory
`ovm_object_utils_begin(i2c_sequence_item)
 `ovm_field_int(addr,OVM_ALL_ON);
`ovm_field_int(data,OVM_ALL_ON);
 `ovm_field_int(wr_rd,OVM_ALL_ON);
`ovm_field_int(ack,OVM_ALL_ON);
 
`ovm_object_utils_end

//constructor
function new(string name = " " );
super.new(name);
endfunction: new

//declaration of constraints

 //constraint address slavedata {data == 8'b10101010};
 
 
endclass : i2c_sequence_item