class i2c_monitor extends ovm_monitor;
i2c_sequence_item seq_item;
virtual i2c_intf intf;
Wrapper wrapper;
ovm_object dummy;
byte data_bytes[$];
bit ackreceived;
ovm_analysis_port #(i2c_sequence_item) mon2sbwrite;
ovm_analysis_port #(i2c_sequence_item) mon2sbread;


//registering
`ovm_component_utils(i2c_monitor)
//constructor
function new(string name = " ", ovm_component parent = null);
super.new(name,parent);
endfunction: new

virtual function void build();
//casting
if(!get_config_object("configuration",dummy,0))
ovm_report_info(get_type_name(),"dummy is not assigned",OVM_LOG);
else
ovm_report_info(get_type_name(),"dummy is assigned",OVM_LOG); 

if($cast(wrapper,dummy))
begin
ovm_report_info(get_type_name(),"object configured properly",OVM_LOG);
intf = wrapper.intf;
mon2sbwrite = new("mon2sbwrite",this);
mon2sbread = new("mon2sbread",this);

end
else
ovm_report_info(get_type_name(),"dummy is really dummy");

endfunction : build

bit start = 0;
bit rdwr,ack;
integer writestate =0;
bit [7:0] temp,data,readdata;
bit [6:0] addr,readaddr; 
virtual task run();
forever
begin
 case (writestate)
  
  0:    begin
  
          wait (intf.sda_in == 1'b0)
		  begin
		     wait (intf.sck == 1'b1)
			  begin
			    $display("start condition detected in monitor at writedatatransfer");
				writestate = 1 ;
				$display("writestate is %d",writestate);
				$display($time);
				@(posedge intf.sck)
				seq_item = i2c_sequence_item::type_id::create("seq_item");

			  end
		  end
        end 
    1:
	     begin
		    
			 
            for(int i =6;i>=0;i--) 
				begin
			    @(negedge intf.sck)	
      
				 addr[i] = intf.sda_in;  
				$display("sda_in at monitor is %d",intf.sda_in);
		     	$display($time);
				end
	        seq_item.addr = addr;
				$display("address at monitor is %d", seq_item.addr);
		    @(negedge intf.sck)
		    rdwr = intf.sda_in;
			
			  if (rdwr == 0) //read write bit
			     begin
				//$display("write data addr %d",rdwr);
				$display("here"); 
				writestate = 2;
				end
	         else
				$display("read data addr %d",rdwr);
	
		 end
	2:
          begin
				@(negedge intf.sck)
					ackreceived = intf.sda_out;
					 if (ackreceived == 0)	
					   begin
						$display("slave address detected");
						@ (negedge intf.sck)

						writestate = 3;
						end
					else 
					    begin
						$display("slave address is wrong at ackreceived ");
						 writestate = 0;
						end
	

          end	

     3:
            begin
                 
				  for(int i=7;i>=0;i--)	
					begin
						@(negedge intf.sck)		
        				data[i]=intf.sda_in;
						
					end
	 
	                seq_item.data =data;
					@ (negedge intf.sck)
                    writestate = 4;
            end		
	4:
            begin
                 
				  		
				  @ (negedge intf.sck)
				  @ (negedge intf.sck)
		         ack = intf.sda_out;
				 $display("at monitor ack from slave for  write data   is %d",ack); 
				  
		         writestate = 5;
            end		

       			
      5:

            begin
                 wait (intf.sck == 1'b1)
					begin
						wait (intf.sda_in == 1'b1)
						begin
							$display("stop condition detected in monitor at writedatatransfer");
							writestate = 6 ;
							mon2sbwrite.write(seq_item);  

			            end
		            end
            end  

 6:    begin
  
          wait (intf.sda_in == 1'b0)
		  begin
		     wait (intf.sck == 1'b1)
			  begin
			    $display("start condition detected in monitor at readdatatransfer");
				writestate = 7 ;
				$display("writestate is %d",writestate);
				$display($time);
				@(posedge intf.sck)
				seq_item = i2c_sequence_item::type_id::create("seq_item");

			  end
		  end
        end 
    7:
	     begin
		    
			 
            for(int i =6;i>=0;i--) 
				begin
			    @(negedge intf.sck)	
      
				 readaddr[i] = intf.sda_in;  
				$display("sda_in at monitor is %d",intf.sda_in);
		     	$display($time);
				end
	        seq_item.addr = readaddr;
				$display("read address at monitor is %d", seq_item.addr);
		    @(negedge intf.sck)
		    rdwr = intf.sda_in;
			
			  if (rdwr == 1) //read write bit
			     begin
				//$display("write data addr %d",rdwr);
				$display("here"); 
				writestate = 8;
				end
	         else
				$display("read data addr %d",rdwr);
	
		 end
	8:
          begin
				@(negedge intf.sck)
					ackreceived = intf.sda_out;
					 if (ackreceived == 0)	
					   begin
						$display("slave address detected");
						@ (negedge intf.sck)

						writestate = 9;
						end
					else 
					    begin
						$display("slave address is wrong at ackreceived ");
						 writestate = 0;
						end
	

          end	

     9:
            begin
                 
				  for(int i=7;i>=0;i--)	
					begin
						@(posedge  intf.sck)
                        $display($time);						
        				readdata[i]=intf.sda_out ;
						$display("at monitor sda_out is %d",intf.sda_out);
						
					end
	 
	                seq_item.data =readdata;
                    writestate = 10;
            end	
	 10:
            begin
                 
				  		
				  @ (negedge intf.sck)
				  @ (negedge intf.sck)
		         ack = intf.sda_in;
				 $display("at monitor ack from master for  read data   is %d",ack); 
				  
		         writestate = 11;
            end		
			
      11:

            begin
                 wait (intf.sck == 1'b1)
					begin
						wait (intf.sda_in == 1'b1)
						begin
							$display("stop condition detected in monitor at readdatatransfer");
							writestate = 0 ;
							mon2sbread.write(seq_item);  

			            end
		            end
            end  
			
endcase

end
endtask

/*
virtual task run();
forever
begin
writedatatransfer();
readdatareceived();
end
endtask

task writedatatransfer ();
 
 case (writestate)
  
  0:    begin
  
          wait (intf.sda_in == 1'b0)
		  begin
		     wait (intf.sck == 1'b1)
			  begin
			    $display("start condition detected in monitor at writedatatransfer");
				writestate = 1 ;
				$display("writestate is %d",writestate);
				seq_item = i2c_sequence_item::type_id::create("seq_item");

			  end
		  end
        end 
    1:
	     begin
		    
			$display("writestate issss %d",writestate);
            for(int i =0;i<=5;i++) 
				begin
			    @(posedge intf.sck)	
      
				seq_item.addr[i] = intf.sda_in;  
				$display("address at monitor is %d",intf.sda_in);
				$display("address at monitor is %d",seq_item.addr);
				end
	        
		    @(posedge intf.sck)
		    rdwr = intf.sda_in;
			
			  if (rdwr == 0) //read write bit
			     begin
				//$display("write data addr %d",rdwr);
				$display("here"); 
				writestate = 2;
				end
	         else
				$display("read data addr %d",rdwr);
	
		 end
	2:
          begin
				@(posedge intf.sck)
					ackreceived = intf.sda_out;
					 if (ackreceived == 1)	
					   begin
						$display("slave address detected");
						writestate = 3;
						end
					else 
					    begin
						$display("slave address is wrong at ackreceived ");
						 writestate = 0;
						end
	

          end	

     3:
            begin
                  @(posedge intf.sck)
				  for(int i=0;i<8;i++)	
					begin
						seq_item.data[i]=intf.sda_in;
					end
	 
                    writestate = 4;
            end			
      4:

            begin
                 wait (intf.sck == 1'b1)
					begin
						wait (intf.sda_in == 1'b1)
						begin
							$display("stop condition detected in monitor at writedatatransfer");
							writestate = 0 ;
							mon2sbwrite.write(seq_item);  

			            end
		            end
            end  
	
endcase
 
endtask
	
	 

task readdatareceived ();

#20ms
 if( intf.sda ==1'b0 && intf.sck == 1'b1 ) begin 
   $display("start condition detected");
   start = 1'b1;
 end else begin
    start= 1'b0;
 end
   
if (start == 1)

begin
seq_item = i2c_sequence_item::type_id::create("seq_item");

    for(int i =0;i<=5;i++) 
	begin
      @(negedge intf.sck)
      seq_item.addr[i] = intf.sda;  
    end
	
	  @(negedge intf.sck)
	  rdwr=intf.sda ;
	  
	  if (rdwr == 0) //read write bit
	    $display("write data addr %d",rdwr);
	  else
	    $display("read data addr %d",rdwr);
		
	  @(posedge intf.sck)
        ackreceived = intf.sda;

      if (ackreceived == 1)	
        $display("slave address detected");
      else 
		$display("slave address is wrong");
		
	  for(int i=0;i<8;i++)	
	  begin
	     seq_item.data[i]=intf.sda;
	  end
	 	
//	$display("data sent during write phase is %d",datareceived);
	
    if( intf.sda ==1'b1 && intf.sck == 1'b1 ) begin 
       $display("stop condition detected ");
	   
	     end
	 else
	   $display("waiting for stop condition");
	   
	 mon2sbread.write(seq_item);  
	 
end	 //start
endtask
	*/	 
endclass : i2c_monitor