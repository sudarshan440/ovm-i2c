class i2c_testcase extends ovm_test;
i2c_sequence seq;
i2c_sequencer sequencer;
i2c_environment env;
//registering
`ovm_component_utils(i2c_testcase)

//constructor
function new(string name = "env", ovm_component parent= null);
super.new(name,parent);
endfunction : new

function void build();
  super.build();
  env = i2c_environment::type_id::create("env",this);
endfunction : build

task run();

sequencer=env.agent.seqencer;
sequencer.count = 0;
  seq = i2c_sequence::type_id::create("seq");
seq.start(sequencer,null);

endtask 

endclass : i2c_testcase