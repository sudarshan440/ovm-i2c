class i2c_sequence extends ovm_sequence#(i2c_sequence_item);
i2c_sequence_item seq_item;

//registering 
`ovm_object_utils(i2c_sequence)

//constructor

function new(string name = " ");
super.new(name);
endfunction 

// body

virtual task body();

//write data to slave
seq_item = i2c_sequence_item::type_id::create("seq_item");
wait_for_grant();
void'(seq_item.randomize() with {seq_item.addr==7'b1001000;seq_item.wr_rd==0;seq_item.data ==8'b10101010;});
send_request(seq_item);
wait_for_item_done();

/*
//read data from slave
seq_item = i2c_sequence_item::type_id::create("seq_item");
wait_for_grant();
void'(seq_item.randomize() with {seq_item.addr==7'b1010100;seq_item.wr_rd==1;});
send_request(seq_item);
wait_for_item_done();
*/


endtask : body
endclass : i2c_sequence