class i2c_environment extends ovm_env;
i2c_agent agent;
scoreboard sc;

//registering
`ovm_component_utils(i2c_environment)

//constructor
function new(string name = " ", ovm_component parent = null);
super.new(name,parent);
endfunction: new

function  void build();
super.build();
agent = i2c_agent::type_id::create("agent",this);
sc = scoreboard::type_id::create("sc",this);
endfunction : build

function void connect();
agent.agent_tb.connect(sc.writesb);
agent.agent_dut.connect(sc.readsb);
endfunction : connect
endclass : i2c_environment