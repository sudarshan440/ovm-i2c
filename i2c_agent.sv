class i2c_agent extends ovm_agent;
i2c_monitor  monitor;
i2c_driver driver;
i2c_sequencer seqencer;
ovm_analysis_port #(i2c_sequence_item) agent_tb;
ovm_analysis_port #(i2c_sequence_item) agent_dut;

//registering
`ovm_component_utils(i2c_agent)

//constructor
function new (string name = " ", ovm_component parent = null);
super.new(name,parent);

endfunction : new

function void build();
super.build();
agent_tb = new("agent_tb",this);
agent_dut = new("agent_dut",this);
seqencer = i2c_sequencer::type_id::create("sequencer",this);
driver = i2c_driver::type_id::create("driver",this);
monitor = i2c_monitor::type_id::create("monitor",this);
endfunction : build

function void connect();
driver.seq_item_port.connect(seqencer.seq_item_export);
monitor.mon2sbwrite.connect(agent_tb);
monitor.mon2sbread.connect(agent_dut);
endfunction : connect


endclass : i2c_agent