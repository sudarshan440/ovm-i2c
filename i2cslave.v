module i2cslave(clk,RST,SCL,SDA_IN,SDA_OUT,DOUT);
input RST,SCL,clk;
input  SDA_IN;
output wire SDA_OUT;
output wire [7:0] DOUT;


wire DADDR = 7'b1001000;
wire ADDR = 8'b00000000;
wire wr =0;
reg [7:0] DOUT_S,data2send =0;
reg  START=0, START_RST=0, STOP=0, ACTIVE=0, ACK=0;
wire SDA_IN;
reg [9:0] SHIFTREG=0;
reg [2:0] STATE=0;
wire sck_negedge,sck_posedge;
reg sclk_reg1 ;
reg [7:0] mem [6:0] ;  
reg [6:0] mem_waddr,mem_raddr ;  
reg rd_en=0,wr_en=0; 
reg [7:0] memory [0:126];
reg [7:0] mem_rdata,mem_wdata,counter=0;
reg sdaoutreg,sdaoutreg0;
integer i; 
 
  
   
/*
   always @ (SDA_IN, START_RST)
   begin
      if (START_RST == 1) 
         START <= 0;    
      else if (SDA_IN == 0) 
         START <= scl;    
      
   end 
   
  always @ (SCL, START, STOP)
   begin
		if (SCL == 0) 
         START_RST <= START;    
     
   end 
*/

//start 
always @  (SDA_IN, START_RST)
begin
	   
	if(RST == 0 )
	  begin
	   if  ( sck_negedge == 0)
	      START <= 0;
	    end
	 else
       begin	 
	   if (sck_posedge == 1 && SDA_IN == 0) 
	      START <= 1;
	   end	  

end

always @ (RST, SCL, SDA_IN, START)
begin
	if (RST == 0)
	  begin
	  if( SCL == 0)
	    begin
		 if  ( START==1) 
		  STOP <= 0;
		 end 
	  end 
	   
	   else
	    begin
		
	   if (sck_posedge == 1) 
	     begin
		  if ( SDA_IN == 1 )
		    STOP <= 1;
	     end
       		 
end 
end

// "active communication" signal 
always @ (RST, STOP, START)
begin
	if (RST == 0)
	  begin
	   if( STOP == 1) 
		ACTIVE <= 0;
	  end	
	else 
	 begin
	  if (START == 0)
		ACTIVE <= 1;
	 end
end 

//data shifter
always@ (RST, ACTIVE, ACK, SCL, SDA_IN )
begin 
if (RST == 0)
  begin
  if ( ACTIVE == 0)
	SHIFTREG <= 10'b0000000001;	
  end	
else 
begin
if (sck_negedge==1)
 begin
	if (ACK == 1)
		SHIFTREG <= 10'b0000000001 ;
	else
		SHIFTREG[9:0] <= {SHIFTREG[8:0], SDA_IN};
 end
end 
end

//I2C data read
always @ (RST, STATE, ACK, SHIFTREG)
begin
if (RST == 0)
	DOUT_S <= 8'b00000000;
else if (STATE==3'b010 && ACK == 1) 
  begin
	DOUT_S <= SHIFTREG[8:1];
	mem_wdata <= SHIFTREG[8:1];
	wr_en <= 1;
	end
 else
  wr_en <= 0;
	
end 
 
 //negedge  
always @(posedge clk)
		begin
			if(RST==0) 
     			sclk_reg1 <=0;
			
			else
				sclk_reg1  <= SCL;
				 
	            
			 
end 
  
  
  
   
  assign	sck_negedge = (~ SCL) &&  ( sclk_reg1);
  assign	sck_posedge = (  SCL) &&  ( ~sclk_reg1); 
  
  
//ACK
always @ (RST, sck_negedge, SHIFTREG, STATE, ACTIVE)
begin
if (RST == 0)
  begin
    if ( ACTIVE == 0)
	begin
	ACK <= 0;
	STATE <= 3'b000;
	end
 end	
else 
begin
 if (sck_negedge==1)
 begin 

case (STATE)

3'b00: //write address
     begin
	    if (SHIFTREG[9] ==  1  )
	      begin
		   if (SHIFTREG[7:0] == 8'b10010000) 
			   begin
			  ACK <= 1;
			  STATE <= 3'b001;
			  mem_waddr <= 7'b1001000;
			  sdaoutreg <= 1;
			  $display("here here ");
			   
			  end
		  end
		 else 
            begin		 
			ACK <= 0;  
			sdaoutreg <= 0;
			end
	 end
	 
3'b001: //write data 
     begin
	    if (SHIFTREG[9] ==  1  )
	      begin
		      ACK <= 1;
			  STATE <= 3'b010;
			  sdaoutreg <= 1;
			  end
		 else 
            begin		 
			ACK <= 0;     
			sdaoutreg <= 0;
			end
			  
	 end
	 
3'b010 :
        begin
          ACK <= 0;
		  STATE <= 3'b011; 
        end	

3'b011 : // read address 

     begin
        if (SHIFTREG[9] ==  1  )
	      begin
		   if (SHIFTREG[7:0] == 8'b10010001) 
			   begin
			  ACK <= 1;
			  STATE <= 3'b100;
			  mem_raddr <= 7'b1001000;
			  sdaoutreg <= 1;
			  $display("  here ");
			   
			  end
		  end
		 else 
            begin		 
			ACK <= 0;
            sdaoutreg <= 0;
            end 			
	 end		
3'b100: //read data  
     begin
	    if (SHIFTREG[9] ==  1  )
	      begin
		      ACK <= 1;
			  STATE <= 3'b000;
			  end
		 else 
            begin		 
			ACK <= 0; 
			sdaoutreg <= 1;
			
			if (counter == 8)
			  counter <= 0;
			else
            begin
            $display("in slave dut read data is mem_rdata[counter] %d",mem_rdata[counter]);
            $display($time); 			
			sdaoutreg0 <= mem_rdata[counter];
			counter <= counter + 1;
			end
         end			
			  
	 end
	 
endcase
 end	 
end
end
 
 assign SDA_OUT = (ACK== 1)?0:( ( sdaoutreg == 1)?  sdaoutreg0:1'bZ);


assign DOUT[7:0] = DOUT_S[7:0];
assign slaveaddress = 7'b1001000;
/*
always  @( SCL, rd_en,wr_en)
begin

   if(rd_en == 1)
   begin
     data2send <= mem[slaveaddress];   
   end

 if (SCL == 1) 
 begin
 if(wr_en == 1)
   begin
    mem[slaveaddress] <= DOUT_S;  
    $display("DOUT_S is %d",DOUT_S);	
	$display(" write data stored at memory is %d",mem[7'b1001000]);
	$display("write address is %d",slaveaddress);
   end 
 end   
  //else  
end	

 */
 
 //write data 
 always@(*) begin
    memory[mem_waddr] <= mem_wdata;
	$display("memory[mem_waddr] write is %d",memory[mem_waddr]);
  end
  
 always@(*) begin
    mem_rdata <= memory[mem_raddr];
	$display("memory[mem_raddr] read is %d",memory[mem_raddr]);
  end
  
  
endmodule







