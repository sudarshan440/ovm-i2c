module top;
  import i2cPkg::*;
  import i2cTestPkg::*;
i2c_intf intf();

 
i2cslave dut (
.clk(intf.clk), 
.RST(intf.rst),
.SCL(intf.sck),
 .DOUT(intf.dout), 
 .SDA_IN(intf.sda_in),
 .SDA_OUT(intf.sda_out) 
		       );

initial
begin
Wrapper wrapper = new("Wrapper");
wrapper.setVintf(intf);

set_config_object("*","configuration",wrapper,0);
run_test("i2c_testcase");
intf.sda_in = intf.sda_out;
#5000;
$finish;

end

 
initial
begin
intf.clk = 0;
end

always #10 intf.clk = ~intf.clk;
 
endmodule : top