class i2c_sequencer extends ovm_sequencer#(i2c_sequence_item);

//registering
`ovm_component_utils(i2c_sequencer)

//constructor
function new(string name = " ", ovm_component parent = null);
super.new(name,parent);
endfunction : new

endclass : i2c_sequencer